package tp4;

public class TestPokemon {

	public static void main(String[] args) {
		Pokemon Sarpetank, Carapuce; 
		Sarpetank=new Pokemon("Sarpetank");
		Carapuce=new Pokemon("Carapuce");
		
		int c=0;
		while(Sarpetank.isAlive()==true) {
			Sarpetank.sePresenter();
			Sarpetank.manger();
			Sarpetank.vivre();
			c=c+1;
		}
		System.out.println(Sarpetank.getNom()+" "+"a vécu"+" "+c+" "+"cycles.");
	}

}
