package tp4;

public class Pokemon {



	//Attributs 

	private int energie;
	private int maxEnergie; 
	public String nom; 
	private int puissance; 

	//Constructeurs

	public Pokemon(String n) {
		nom=n;
		maxEnergie=50+(int)(Math.random()*((90-50)+1));
		energie=30; 
		puissance=(3+(int)(Math.random()*((10-3)+1)));
	}

	//Accesseurs

	String getNom() {
		return nom;
	}

	int getEnergie() {
		return maxEnergie; 
	}

	int getPuissance() {
		return puissance; 
	}


	//Méthodes 

	void sePresenter() {
		System.out.println("Je suis"+" "+nom+" "+"j'ai"+" "+energie+" "+"points d'energies"+" "+"("+maxEnergie+")max et une puissance de"+" "+puissance);
	}

	void manger() {
		if(energie<maxEnergie && energie>0) {
			energie=energie+(10+(int)(Math.random()*((30-10)+1)));

			if (energie>maxEnergie) 
				energie=maxEnergie; 
		}
		else {		
			energie=maxEnergie;
		}

	}

	void vivre() {
		energie=energie-(20+(int)(Math.random()*((40-20)+1)));
		if (energie<0) {
			energie=0;

		}
	}

	boolean isAlive() {
		if(energie==0) {
			return false;
		}
		else {
			return true;
		}
	}

	void perdreEnergie(int perte) {
		energie=energie-perte; 
	}
	
}
