package tp5;

import javax.swing.JFrame; 
import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.Container; 
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JTextField; 
import java.awt.GridLayout;


public class MonAppliGraphique extends JFrame{   //Permet d'importer le Jframe a cette class.

	//Attributs 
	
	JButton b = new JButton("Bouton 0");
	JButton b1 = new JButton("Bouton 1");
	JButton b2 = new JButton("Bouton 2");
	JButton b3 = new JButton("Bouton 3");
	JButton b4 = new JButton("Bouton 4");
	
	JLabel monLabel = new JLabel("Je suis un JLabel"); 
	
	JTextField monTextField = new JTextField("Je suis un JTextField"); 
	
	
	
	//Constructeurs
	
	public MonAppliGraphique () {
		super();   //Permet d'héréditer les paramètres du constructeur. 
		this.setTitle("Ma première application"); //Permet de donner un nom a l'application 
		this.setSize(400,200); //Permet de  dimensionner la fenêtre 
		this.setLocation(20,20); //Permet de position l'application sur l'écran 
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true); 
		System.out.println("La fenêtre est créée !");
		Container panneau = getContentPane(); // Permet de récupéré la fenêtre
		panneau.add(b); //Création du bouton 0
		panneau.setLayout(new GridLayout(3,2)); //Affiche les composants 
		panneau.add(b1, BorderLayout.NORTH); //Création du bouton 1
		panneau.add(b2, BorderLayout.EAST); //Création du bouton 2
		panneau.add(b3, BorderLayout.SOUTH); //Création du bouton 3
		panneau.add(b4, BorderLayout.WEST); //Création du bouton 4
		//panneau.add(monLabel); 
		//panneau.add(monTextField); 
	}
	

	
	//Successeurs 
	
	
	
	
	//Méthodes 
	
		
	//Méthode Main 
	
	public static void main(String[] args) {
		MonAppliGraphique app = new MonAppliGraphique (); //Création de l'application 	 
	}
}
