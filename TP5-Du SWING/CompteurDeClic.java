package tp5;

import javax.swing.JFrame;
import java.awt.FlowLayout;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.*;

public class CompteurDeClic extends JFrame implements ActionListener {


	//Attributs

	JButton c = new JButton("Click!"); 

	JLabel nombre = new JLabel("Vous avez cliqué 0 fois !"); 
	int a=0;

	//Constructeur

	public CompteurDeClic () {

		super();   
		this.setTitle("CompteurDeClic");  
		this.setSize(200,100); 
		this.setLocation(20,20); 
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		System.out.println("Le Compteur de clic est créé");
		Container panneau = getContentPane();
		panneau.add(c); 
		panneau.setLayout(new FlowLayout());
		panneau.add(nombre);
		c.addActionListener(this);
		this.setVisible(true);
	}

	//Accesseurs







	//Méthodes  



	public void actionPerformed(ActionEvent e) {
		System.out.println("Une Action est détectée");
		a=a+1; 
		nombre.setText("Vous avez cliqué"+" "+a+" "+"fois !"); 

	}
	
	//Méthode Main
	public static void main(String[] args) {
		CompteurDeClic app = new CompteurDeClic (); //Création de l'application 

	}
}