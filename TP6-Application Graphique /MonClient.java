package tp6;

import java.io.PrintWriter;
import java.net.Socket;

public class MonClient {

	public static void main(String[] args) {
		Socket monSocket;
		PrintWriter monPrintWriter; 
		try {
			monSocket = new Socket("localhost", 8888); 
			System.out.println("Client: " + monSocket);
			//monSocket.close(); 
			
			monPrintWriter = new PrintWriter(monSocket.getOutputStream());
			System.out.println("Envoi du message : Hello World");
			monPrintWriter.println("Hello World");
			monPrintWriter.flush(); 
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}
