package tp6;

public class TestExceptions {
	
	public static void main(String[] args) {
		int x = 2, y = 0; //On initie la valeur 2 pour x et la valeur 0 pour y
		try { //Ce bloc relève une exception de la division par 0 
			System.out.println("y/x = " + y/x);
			System.out.println("x/y = " + x/y);
			System.out.println("Commande de fermeture du programme");
		}
		catch (Exception e) { //Ce bloc montre le traitement de l'exception se faisant dans le bloc
			System.out.println("Une exception a été capturée");
		}
		System.out.println("Fin du programme");
	}

}
