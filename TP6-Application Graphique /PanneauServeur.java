package tp6;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener; 
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea; 
import javax.swing.JScrollPane;

public class PanneauServeur extends JFrame implements ActionListener {

	//Attributs
	
	JButton b0 = new JButton("Exit");
	
	JTextArea b1 = new JTextArea();
	
	//Constructeurs 

	public PanneauServeur () {
		super (); 
		this.setTitle("Serveur-Panneau d'affichage");
		this.setSize(400,300); 
		this.setLocation(20,20); 
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		 
		Container panneau = getContentPane();
		
		 
		panneau.add(b0, BorderLayout.SOUTH); 
		b0.addActionListener(this);
		
		panneau.add(b1); 
		b1.append("Le panneau est actif");
		
		this.setVisible(true);
		

	}


	//Méthode Main 

	public static void main(String[] args) {
		PanneauServeur app = new PanneauServeur ();
	}
	
	public void actionPerformed (ActionEvent e) {
		System.exit(-1);
	}
}
