package tp2;

public class Cercle extends Forme{
	
	//Attributs
	
	private double rayon;
		
	
	//Constructeurs
	
	Cercle(){
		super();
		rayon = 1.0;
	}
	

	//Accesseurs 
	
	void setRayon(double r) {
		rayon = r;
	}
		
	double getRayon() {
		return rayon;
	}
	
	

	//Méthodes
	
	public String seDecrire() {
		return "Un cercle de rayon"+" "+rayon+" "+"est issue d'une Forme de couleur"+ " "+getCouleur()+" "+"et de coloriage"+" "+isColoriage();
	}


}

// A finir