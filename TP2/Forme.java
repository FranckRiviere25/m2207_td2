package tp2;

public class Forme {

	//Attributs 

	private String couleur;

	private boolean coloriage;


	//Constructeurs 

	public Forme() {
		couleur="orange";
		coloriage= true; 
	}

	public Forme(String c, boolean r) {
		couleur=c;
		coloriage=r; 
	}


	//Accesseurs 

	String getCouleur() {
		return couleur; 
	}

	void setCouleur(String c) {
		couleur=c;
	}

	boolean isColoriage () {
		return coloriage; 
	}

	void setColoriage(boolean b) {
		coloriage=b; 
	}


	//Méthodes

	public String seDecrire() {
		return " une Forme de couleur "+ couleur + " " + "et de coloriage "+ coloriage;
	}

}