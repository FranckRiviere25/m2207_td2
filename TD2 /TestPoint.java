package td2;

public class TestPoint {
	
	 	//Exercice 1.1 a 1.3

	public static void main(String[] args) {
		Point p;
		p= new Point (7,2);

		System.out.println("x=" + p.getX());
		System.out.println("y=" + p.getY());
		
		p.setX(5);
		System.out.println("La nouvelle valeur de x est : " + p.getX());
		

		//Exercice 1.4
		System.out.println("Exercice 1.4");
		System.out.println("x=" + p.x);
		System.out.println("y=" + p.y); 
		
		//Exercice 1.8
		System.out.println("Exercice 1.8");
		System.out.println("x=" + p.getX());
		System.out.println("y=" + p.getY());
		
		p.deplacer(-2, 3);
	}

}
