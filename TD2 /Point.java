package td2;

public class Point {

	//Attributs
	public int x,y;

	//constructeurs 
	public Point(int x1, int y2) {
		x= x1;
		y= y2;
	}

	//Accesseurs 
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x=x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y=y;
	}


	//Méthodes
	public void deplacer(int a,int b) {
		x = x + a;
		y = y + b;
	}
}