package tp1;

public class Compte {



	//Attributs

	private int numero;
	private double solde;
	private double decouvert; 


	//Constructeur

	public Compte (int numero) {
		this.numero=numero;	
		solde=0;
		decouvert=0; 
	} 
	//Accesseurs
	void setDecouvert(double montant) {
		decouvert=montant;
	}

	double getDecouverte() {
		return decouvert;
	}

	int getNumero() {
		return numero;
	}

	double getSolde() {
		return solde;
	}

	//Méthodes
	void afficherSolde() {
		System.out.println("Solde:"+" "+getSolde());
	}

	void depot(double montant) {
		solde=solde+montant;
	}

	String retrait(double montant) {
		if(solde>decouvert+solde) {
			solde-=solde;
			System.out.println("Refuser");
		}
		else {
			solde-=montant;
			System.out.println("Accepter");
		}
		return null; 
	}
}