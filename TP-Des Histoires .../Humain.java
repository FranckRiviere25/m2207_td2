package tp3;

public class Humain {
	
	
	//Attributs 
	protected String nom;
	protected String boissonfavorite; 
	
	
	//Constructeurs
	
	public Humain(String n) {
		nom=n;
		boissonfavorite = "lait";  
	}
	
	
	//Successeurs
		
	//Méthodes
	
	public String quelEstTonNom() {
		return nom; 
	}
	
	public String quelleEstTaBoisson() {
		return boissonfavorite; 
	}
	
	void parler(String texte) {
		parler("("+nom+")" + "-" ); 
	}

	
	void sePresenter() {
		parler("Je suis"+" "+nom+" "+"et ma boisson préférée est le"+" "+boissonfavorite+".");
	}
	
	void boire() {
		parler("Ah ! Un bon verre de"+" "+boissonfavorite+"! GLOUPS !");
	}
	
	
	

}