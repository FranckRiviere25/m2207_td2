package tp3;

public class Brigand extends Humain {
	
	//Attributs
	
	private String look;
	private int nbdame; 
	private int recompense; 
	private boolean libre; 
	
	//Constructeurs
	
	Brigand(String s) {
		super(s); 
		boissonfavorite="Cognac";
		look="Chasseur";
		nbdame=12;
		recompense=50000;
		libre=true;
		
	}
	
	
	//Méthodes
	
	public int getRecompense() {
		return recompense;
	}	
	
	public String quelEstTonNom() {
		parler(nom+" "+" "+look);
	}
	
}