package tp3;

public class Dames extends Humain {
	
	//Attributs
	
	private boolean capture; 
	
	//Constructeurs
	
	public Dames(String s) {
		super(s);
		boissonfavorite="Martini"; 
	}
		
	//Méthodes


	void priseEnOtage() {
		capture = false;
		parler("Au secours !!");
	}
	
	void estLiberee() {
		capture = true;
		parler("Merci Cowboy");
	}
	
	public String quelEstTonNom() {
		parler("Miss"+" "+ nom+" "+".");
	}
	
}
